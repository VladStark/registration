package com.example.shredder.loginandpassword;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    protected SharedPreferences mSharedPref;
    //SharedPreferences
    protected String USERNAME = "Сохраненное имя";
    protected String PASSWORD = "Сохраненный пароль";
    //Ключи
    private String logincheck = "";
    private String passwordcheck = "";
    //
    protected boolean number,bigword,specsimvol,smallword,checker,power;
    protected int Counter = 0;
    //Для password'a(Checker для проверки в ProfileActivity) и power для checkbox
    protected CheckBox sPassword;
    protected Button bRegister,bSave;
    protected EditText etLogin, etPassword,etReturnPassword;
    //Объявление

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bSave = (Button) findViewById(R.id.button_save);
        bRegister = (Button) findViewById(R.id.b_register);
        etLogin = (EditText) findViewById(R.id.et_login);
        etPassword  = (EditText) findViewById(R.id.et_password);
        etReturnPassword = (EditText) findViewById(R.id.et_returnpassword);
        sPassword = (CheckBox) findViewById(R.id.checkBox);

        bRegister.setOnClickListener(this);
        bSave.setOnClickListener(this);

        startCheckBox();
    } //Тут происходит Инициализация

    protected void startCheckBox() {
        sPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    power = true;
                    // Переключатель включен
                } else {
                    power = false;
                    //Переключатель выключен
                }
            }
        });
    } // Обработчик checkbox

    protected void intentHelper() {
        Intent intent = new Intent(this, ProfileActivity.class);
        intent.putExtra("login", etLogin.getText().toString());
        intent.putExtra("password", etPassword.getText().toString());
        intent.putExtra("checker", checker);
        intent.putExtra("power", power);
        intent.putExtra("marks", Counter);
        startActivity(intent);
    }

    protected void passwordChecker () {

        number = false;
        bigword = false;
        specsimvol = false;
        smallword = false;
        checker = false;

        String str = etPassword.getText().toString();
        char[] array = str.toCharArray();
        char [] specarray = {'~','!','@','#','$','%','^','&','*','?',',','.','<','>','/','-','=','+','_',':','"',';','[',']','{','}','|','(',')','№'};
        for (int i = 0;i < array.length;i++) {
            if (Character.isUpperCase(array[i])) {
                bigword = true;
            }
             else if (Character.isDigit(array[i])) {
            number = true;
        }
            else if (Character.isLowerCase(array[i])) {
                smallword = true;
            }
            for (int j = 0;j < specarray.length;j++) if (array[i] == specarray[j]) specsimvol = true;
        }

        if (bigword && number && smallword && specsimvol) Counter = 4;
        else if (bigword && number && smallword) Counter = 3;
        else if (bigword && number && specsimvol) Counter = 3;
        else if (smallword && number && specsimvol) Counter = 3;
        else if (bigword && smallword && specsimvol) Counter = 3;
        else if (bigword && number) Counter = 2;
        else if (smallword && number) Counter = 2;
        else if (bigword && smallword) Counter = 2;
        else if (bigword && specsimvol) Counter = 2;
        else if (smallword && specsimvol) Counter = 2;
        else if (number && specsimvol) Counter = 2;
        // Без коммент. про это креативное решение)
        loadAccount();
        if((etPassword.getText().length()!=0) && (etLogin.getText().length()!=0)) {
            if (etPassword.getText().length() >= 6 && etPassword.getText().toString().equals(etReturnPassword.getText().toString()) && Counter >= 2) {
                if (power) {
                    if (logincheck.equals(etLogin.getText().toString()) && passwordcheck.equals(etPassword.getText().toString())) {
                        checker = true;
                        intentHelper();
                    } else {
                        mSharedPref = getPreferences(MODE_PRIVATE);
                        SharedPreferences.Editor mEditor = mSharedPref.edit();
                        mEditor.putString(USERNAME, etLogin.getText().toString());
                        mEditor.putString(PASSWORD, etPassword.getText().toString());
                        mEditor.commit();
                        mEditor.apply();
                        intentHelper();
                    }
                } else {
                    if (logincheck.equals(etLogin.getText().toString()) && passwordcheck.equals(etPassword.getText().toString())) {
                        checker = true;
                        intentHelper();
                    } else {
                        intentHelper();
                    }
                }
            }
            else if (!(etPassword.getText().toString().equals(etReturnPassword.getText().toString()))) {
                Toast.makeText(getApplicationContext(), "Перепроверьте пароль", Toast.LENGTH_SHORT).show();

            }
            else {
                Toast.makeText(getApplicationContext(),
                        "Пароль должен состоять из 6 и более символов и как минимум из 2 различных групп символов!", Toast.LENGTH_LONG).show();
            }
        }
    } //Проверяет логин и пароль(В нем реализовано сохранение аккаунта)

    private void validator () {
        if((etPassword.getText().length()!=0) && (etLogin.getText().length()!=0) && (etReturnPassword.getText().length()!= 0) ){
            passwordChecker();
        }
        else {
            Toast.makeText(getApplicationContext(), "Заполните оставшиеся поля!", Toast.LENGTH_SHORT).show();
        }
    } //Проверяет пустые поля

    private void loadAccount() {
        mSharedPref = getPreferences(MODE_PRIVATE);
        logincheck = mSharedPref.getString(USERNAME, "");
        passwordcheck = mSharedPref.getString(PASSWORD, "");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.b_register:
                validator();
                Counter = 0;
                break;
            case R.id.button_save:
                loadAccount();
                etLogin.setText(logincheck);
                etPassword.setText(passwordcheck);
                etReturnPassword.setText(passwordcheck);

        }
    }
}