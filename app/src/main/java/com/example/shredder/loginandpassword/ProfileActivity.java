package com.example.shredder.loginandpassword;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Intent intent = getIntent();
        String login = intent.getExtras().getString("login");
        String password = intent.getExtras().getString("password");
        int marks = intent.getExtras().getInt("marks");
        boolean power = intent.getExtras().getBoolean("power");
        boolean checker = intent.getExtras().getBoolean("checker");
        TextView text = new TextView(this);
        if (power && !checker ) {
            text.setText("Вы успешно зарегистрировались и аккаунт ваш сохранен" +
                    "\n" + "Ваш логин: " + login +
                    "\n" + "Ваш пароль: " + password +
                    "\n" + "Оценка вашей безопасности : " + marks + " из 4");
        }
        else if (checker) {
            text.setText("Вы уже зарегистрированы." +
                    "\n"+"Перенаправление......." +
                            "\n"+ "Вы вошли в систему под логином : " + login +
                    "\n" + "Ваш пароль: " + password);
        }
        else {
            text.setText("Вы успешно зарегистрировались,но ваш аккаунт не сохранен" +
                    "\n" + "Ваш логин: " + login +
                    "\n" + "Ваш пароль: " + password +
                    "\n" + "Оценка вашей безопасности : " + marks + " из 4");
        }
        setContentView(text);
    }

}